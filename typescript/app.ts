/// <reference path="_all.ts" />

module app {
	'use strict';

	let confApp: ng.IModule = angular.module('confApp', [
		'ngRoute',
		'ui.calendar'
	]);

	confApp.controller('AppCtrl', AppCtrl);
	confApp.config(['$routeProvider',
			function($routeProvider: ng.route.IRouteProvider): void {
				$routeProvider.
					when('/', {
						controller: 'AppCtrl',
					}).otherwise({
						redirectTo: '/'
					});
			}
	]);
}
