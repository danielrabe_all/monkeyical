/// <reference path="_all.ts" />
var app;
(function (app) {
    'use strict';
    var confApp = angular.module('confApp', [
        'ngRoute',
        'ui.calendar'
    ]);
    confApp.controller('AppCtrl', app.AppCtrl);
    confApp.config(['$routeProvider',
        function ($routeProvider) {
            $routeProvider.
                when('/', {
                controller: 'AppCtrl',
            }).otherwise({
                redirectTo: '/'
            });
        }
    ]);
})(app || (app = {}));
