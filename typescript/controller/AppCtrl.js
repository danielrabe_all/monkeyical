/// <reference path='../_all.ts' />
var app;
(function (app) {
    'use strict';
    var AppCtrl = (function () {
        function AppCtrl($scope, $http, $location) {
            this.$scope = $scope;
            this.$http = $http;
            this.$location = $location;
            $scope.eventSources = [];
            $scope.Lang = 'de';
            $scope.message = '';
            $scope.targetObject = '';
            $scope.input = '';
            $scope.dayClick = function () {
                console.log("day clicked");
            };
            $scope.refreshStrings = function () {
                var targetString = '...';
                if ($scope.targetObject) {
                    targetString = $scope.language[$scope.Lang][$scope.targetObject];
                }
                $scope.message = $scope.language[$scope.Lang][$scope.input].replace('#', targetString);
            };
            $scope.$watch(function (scope) {
                return scope.input;
            }, function (newValue, oldValue) {
                if (newValue != oldValue) {
                    $scope.refreshStrings();
                }
            });
            $scope.$watch(function (scope) {
                return scope.targetObject;
            }, function (newValue, oldValue) {
                if (newValue != oldValue) {
                    $scope.refreshStrings();
                }
            });
            $scope.$watch(function (scope) {
                return scope.Lang;
            }, function (newValue, oldValue) {
                if (newValue != oldValue) {
                    $scope.refreshStrings();
                }
            });
            $scope.setScumm = function (input) {
                $scope.input = input;
            };
            $scope.setTarget = function (input) {
                $scope.targetObject = input;
            };
            $scope.eventDrop = function () {
                console.log("event droped");
            };
            $scope.resize = function () {
                console.log("resize");
            };
            $scope.uiConfig = {
                calendar: {
                    height: 400,
                    editable: true,
                    header: {
                        left: 'month basicWeek basicDay agendaWeek agendaDay',
                        center: 'title',
                        right: 'today prev,next'
                    },
                    dayClick: $scope.dayClick,
                    eventDrop: $scope.eventDrop,
                    eventResize: $scope.resize
                }
            };
            $scope.language = {
                'de': {
                    'add-as-appointment': 'als Termin hinzufügen',
                    'add-as-task': 'als Aufgabe hinzufügen',
                    'appointment': "Termin",
                    'task': "Aufgabe",
                    'open': 'öffne #',
                    'create': 'lege # an',
                    'duplicate': 'dupliziere #',
                    'delete': 'lösche #',
                    'move': 'verschiebe #',
                    'Open': 'Öffnen',
                    'Create': 'Anlegen',
                    'Duplicate': 'Duplizieren',
                    'Delete': 'Löschen',
                    'Move': 'Verschieben',
                    'location': 'Adresse',
                    'person': 'Kontaktdaten',
                    'note': 'Notiz'
                },
                'en': {
                    'add-as-appointment': 'add as appointment',
                    'add-as-task': 'add as task',
                    'appointment': "appointment",
                    'task': "Task",
                    'open': 'open #',
                    'create': 'create #',
                    'duplicate': 'duplicate #',
                    'delete': 'delete #',
                    'move': 'move #',
                    'Open': 'Open',
                    'Create': 'Create',
                    'Duplicate': 'Duplicate',
                    'Delete': 'Delete',
                    'Move': 'Move',
                    'location': 'location',
                    'person': 'contact-data',
                    'note': 'note'
                }
            };
        }
        AppCtrl.$inject = [
            '$scope',
            '$http',
            '$location'
        ];
        return AppCtrl;
    })();
    app.AppCtrl = AppCtrl;
})(app || (app = {}));
